﻿using System;
// struct for save point
public struct Point
{
    public double X { get; set; }
    public double Y { get; set; }

    public Point(double x, double y) : this()
    {
        X = x;
        Y = y;
    }
}

public interface IRectangle
{
    Point LeftTopPoint { get; }
    Point RightBottomPoint { get; }
    void MoveRectangle(Point offset);
    void MoveLeftTopPoint(Point offset);
    void MoveRightBottomPoint(Point offset);
    IRectangle MergeRectangle(IRectangle rectangle);
    IRectangle IntersectionRectangle(IRectangle rectangle);
}

public class Rectangle : IRectangle
{
    public Point LeftTopPoint { get; private set; }
    public Point RightBottomPoint { get; private set; }

    public Rectangle(Point leftTopPoint, double width, double height)
    {
        LeftTopPoint = leftTopPoint;
        RightBottomPoint = new Point(leftTopPoint.X + width, leftTopPoint.Y - height);
    }

    public Rectangle(Point leftTopPoint, Point rightBottomPoint)
    {
        if(leftTopPoint.X>=rightBottomPoint.X||rightBottomPoint.Y>=leftTopPoint.Y)
            throw new ArgumentException("Incorrect value of the points of the rectangle.");
        LeftTopPoint = leftTopPoint;
        RightBottomPoint = rightBottomPoint;
    }
    // offset value of point
    public void MoveRectangle(Point offset)
    {
        LeftTopPoint = new Point(LeftTopPoint.X + offset.X, LeftTopPoint.Y + offset.Y);
        RightBottomPoint = new Point(RightBottomPoint.X + offset.X, RightBottomPoint.Y + offset.Y);
    }
    //change size rectangle
    public void MoveLeftTopPoint(Point offset)
    {
        if (LeftTopPoint.X + offset.X >= RightBottomPoint.X || LeftTopPoint.Y + offset.Y <= RightBottomPoint.Y)
        {
            throw new ArgumentException("Large offset values.");
        }
        LeftTopPoint = new Point(LeftTopPoint.X + offset.X, LeftTopPoint.Y + offset.Y);
    }
    //change size rectangle
    public void MoveRightBottomPoint(Point offset)
    {
        if (RightBottomPoint.X + offset.X <= LeftTopPoint.X || RightBottomPoint.Y + offset.Y >= LeftTopPoint.Y)
        {
            throw new ArgumentException("Large offset values.");
        }
        RightBottomPoint = new Point(RightBottomPoint.X + offset.X, RightBottomPoint.Y + offset.Y);
    }
    //return marge rectangle
    public IRectangle MergeRectangle(IRectangle rectangle)
    {
        Point leftTopPoint = new Point();
        Point rightBottomPoint = new Point();

        leftTopPoint.X = Math.Min(this.LeftTopPoint.X, rectangle.LeftTopPoint.X);
        leftTopPoint.Y = Math.Max(this.LeftTopPoint.Y, rectangle.LeftTopPoint.Y);
        rightBottomPoint.X = Math.Max(this.RightBottomPoint.X, rectangle.RightBottomPoint.X);
        rightBottomPoint.Y = Math.Min(this.RightBottomPoint.Y, rectangle.RightBottomPoint.Y);
        return new Rectangle(leftTopPoint,rightBottomPoint);
    }
    //return marge intersection
    public IRectangle IntersectionRectangle(IRectangle rectangle)
    {
        Point leftTopPoint = new Point();
        Point rightBottomPoint = new Point();

        leftTopPoint.X = Math.Max(this.LeftTopPoint.X, rectangle.LeftTopPoint.X);
        leftTopPoint.Y = Math.Min(this.LeftTopPoint.Y, rectangle.LeftTopPoint.Y);
        rightBottomPoint.X = Math.Min(this.RightBottomPoint.X, rectangle.RightBottomPoint.X);
        rightBottomPoint.Y = Math.Max(this.RightBottomPoint.Y, rectangle.RightBottomPoint.Y);

        if (leftTopPoint.X >= rightBottomPoint.X || rightBottomPoint.Y >= leftTopPoint.Y)
           return null;
        return new Rectangle(leftTopPoint,rightBottomPoint);
    }

}

class Program
{
    static void PrintInfoRectangle(IRectangle rectangle)
    {
        Console.WriteLine("Rectangle points:");
        Console.WriteLine("Left top: ({0},{1})", rectangle.LeftTopPoint.X, rectangle.LeftTopPoint.Y);
        Console.WriteLine("Left bottom: ({0},{1})", rectangle.LeftTopPoint.X, rectangle.RightBottomPoint.Y);
        Console.WriteLine("Right top: ({0},{1})", rectangle.RightBottomPoint.X, rectangle.LeftTopPoint.Y);
        Console.WriteLine("Right bottom: ({0},{1})", rectangle.RightBottomPoint.X, rectangle.RightBottomPoint.Y);
        Console.WriteLine();
    }

    static void Main()
    {
        try
        {
            Console.WriteLine("___Task4___");
            Console.WriteLine();

            Console.WriteLine("**Create rectangle**");
            IRectangle rectangle1 = new Rectangle(new Point(-2, 5), new Point(7, 1));
            IRectangle rectangle2 = new Rectangle(new Point(4, 8), 5, 5);
            PrintInfoRectangle(rectangle1);
            PrintInfoRectangle(rectangle2);
            Console.WriteLine();

            Console.WriteLine("**Intersection rectangle**");
            IRectangle rectangle3 = rectangle1.IntersectionRectangle(rectangle2);
            PrintInfoRectangle(rectangle3);
            Console.WriteLine();

            Console.WriteLine("**Merge rectangle**");
            IRectangle rectangle4 = rectangle1.MergeRectangle(rectangle2);
            PrintInfoRectangle(rectangle4);
            Console.WriteLine();

            Console.WriteLine("**Change size rectangle**");
            rectangle1.MoveLeftTopPoint(new Point(2, 1));
            PrintInfoRectangle(rectangle1);

            Console.WriteLine("**Change size rectangle**");
            rectangle1.MoveRightBottomPoint(new Point(-1, 1));
            PrintInfoRectangle(rectangle1);

            Console.WriteLine("**Move rectangle**");
            rectangle1.MoveRectangle(new Point(2, 2));
            PrintInfoRectangle(rectangle1);

            Console.ReadKey();

        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            //throw;
        }



    }
}

